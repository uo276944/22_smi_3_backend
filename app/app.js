const express = require('express');
const videos = require('./routes/video.routes');
const auth = require('./routes/auth.routes');
const cors = require('cors');
const bodyParser = require("body-parser");
const winston = require('winston');
const expressWinston = require('express-winston');
const db = require("./models");
const { getAllUsers } = require('./controllers/video.controller');

const app = express();

// Synchronize models with the database
db.sequelize.sync();

// Logging messages when new requests are received at the server
// (used to monitor, debug and diagnose the service) 
app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console()
    ]
  }));

// Avoid CORS issues
app.use(cors());

// Parse requests of content-type: application/json
app.use(bodyParser.json());

// Parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  //res.send('Almacén de vídeos');
  
  res.render('This is an API to a video web')
});


// Routes
app.use('/videos', videos);
app.use('/', auth);
app.use('/users', getAllUsers);


module.exports = app;