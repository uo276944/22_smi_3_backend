#!/bin/bash


ffmpeg -i $1 -c:v libx264 -r 24 -g 24 -b:v 1000k -maxrate 1000k -bufsize 2000k -strict -2 $1-1000k.mp4
ffmpeg -i $1 -c:v libx264 -r 24 -g 24 -b:v 500k -maxrate 500k -bufsize 1000k -strict -2 $1-500k.mp4
ffmpeg -i $1 -c:v libx264 -r 24 -g 24 -b:v 250k -maxrate 250k -bufsize 500k -strict -2  $1-250k.mp4

bento4/bin/mp4fragment --fragment-duration 2000 $1-1000k.mp4 $1-1000k-frag.mp4
bento4/bin/mp4fragment --fragment-duration 2000 $1-500k.mp4  $1-500k-frag.mp4
bento4/bin/mp4fragment --fragment-duration 2000 $1-250k.mp4  $1-250k-frag.mp4

python bento4/utils/mp4-dash.py --use-segment-timeline -o . $1-1000k-frag.mp4 $1-500k-frag.mp4 $1-250k-frag.mp4