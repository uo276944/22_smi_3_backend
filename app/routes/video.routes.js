const express = require('express');
const router = express.Router();
const controller = require("../controllers/video.controller.js");
const validator = require('../middlewares/validators/video.validator');
const fileUpload = require('express-fileupload');

// Parse requests of content-type: multipart/form-data
router.use(fileUpload({ createParentPath: true }))

// Get all videos
router.get('/', controller.getAll);

// Create a new video
router.post('/', validator.create, controller.create);

// Get a single video
router.get('/:id', controller.get);

// Upload a video film image
//router.post('/:id/upload', validator.upload, controller.upload)

module.exports = router;
