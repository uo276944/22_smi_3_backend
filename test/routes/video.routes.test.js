const app = require('../../app/app');
const chai = require('chai');
const expect = require('chai').expect;
const utils = require('./utils');
const fs = require('fs');
const sinon = require('sinon');
const mediaEncoding = require('../../app/media/encoding');

chai.use(require('chai-http'));
chai.use(require('chai-arrays'));

const VIDEO_URI = '/videos';


describe('Get videos: ', () => {

    /**
     * Populate database with some data before all the tests in this suite.
     */
    before(async () => {        
        await utils.populateVideos();
    });

    /**
     * This is run once after all the tests.
     */
    after(async () => {
        await utils.dropVideos();
    });

    /**
     * Get all videos correctly
     */
    it('should get all videos', (done) => {
        chai.request(app)
            .get(VIDEO_URI)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.array();
                expect(res.body.length).to.equal(2);
                done();
            });
    });

    /**
     * Get an existing video correctly
     */
    it('should get a video', (done) => {
        chai.request(app)
            .get(VIDEO_URI + '/2')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body.id).to.be.equal(2);
                expect(res.body.title).to.be.equal('La vida es sueño');
                expect(res.body.description).to.be.equal('Calderón de la Barca');
                done();
            });
    });
});


// Auth token
let token;

describe('Create videos: ', () => {

    /**
     * Populate database with some data before all the tests in this suite.
     */
     before(async () => {
        await utils.populateVideos();
        await utils.populateUsers();
        token = await utils.login('Username1', 'Password1' );
    });

    /**
     * This is run once after all the tests.
     */
    after(async () => {
        await utils.dropVideos();
        await utils.dropUsers();
    });

    /**
     * A video should be created correctly
     */
    it('should create a valid video', (done) => {
        const title = "Harry Potter y la piedra filosofal";
        const description = "J.K. Rowling";
        chai.request(app)
            .post(VIDEO_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title: title, description: description })
            .end((err, res) => {
                expect(res).to.have.status(201);
                expect(res).to.be.json;
                expect(res.body.title).to.be.equal(title);
                expect(res.body.description).to.be.equal(description);
                expect(res.body.id).to.be.equal(3);
                done();
            });
    });

    /**
     * An invalid title should raise an error
     */
    it('should receive an error with an invalid title', (done) => {
        chai.request(app)
            .post(VIDEO_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title:"", description: "Unknown" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

    /**
     * A missing title should raise an error
     */
    it('should receive an error with missing name', (done) => {
        chai.request(app)
            .post(VIDEO_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ description: "Unknown" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

    /**
     * An invalid description should raise an error
     */
    it('should receive an error with an invalid description', (done) => {
        chai.request(app)
            .post(VIDEO_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title:"A valid title", description: "No" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });

    /**
     * A missing description should raise an error
     */
    it('should receive an error with missing description', (done) => {
        chai.request(app)
            .post(VIDEO_URI)
            .set('Authorization', 'Bearer ' + token)
            .send({ title: "A valid title" })
            .end((err, res) => {
                expect(res).to.have.status(422);
                done();
            });
    });
});

describe('Upload video covers: ', () => {

    /**
     * Populate database with some data before all the tests in this suite.
     */
    before(async () => {
        await utils.populateVideos();
        await utils.populateUsers();
        token = await utils.login('Username1', 'Password1');

        // Mock normalize function
        sinon.stub(mediaEncoding, 'normalize').resolves('/covers/cover-2.png'); // Do nothing
    });

    /**
     * This is run once after all the tests.
     */
    after(async () => {
        await utils.dropVideos();
        await utils.dropUsers();

        // Restore normalize function
        mediaEncoding.normalize.restore();
    });

    /**
     * A video cover should be uploaded correctly
     */
    it('should upload a video cover', (done) => {
        chai.request(app)
            .post(VIDEO_URI + '/2/upload')
            .set('Authorization', 'Bearer ' + token)
            .attach('FilmFile', fs.readFileSync('./test/assets//film.mp4'), 'film.mp4')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body.id).to.be.equal(2);
                expect(res.body.title).to.be.equal('La vida es sueño');
                expect(res.body.description).to.be.equal('Calderón de la Barca');
                expect(res.body.cover).to.be.equal('/film/film-2.png');
                done();
            });
    }).timeout(5000);  // Timeout 5 secs


    /**
     * TODO: tests for invalid upoloads
     */
});
